package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

/**
 * keywords on detail page
 */
public class ImdbKeyword extends BaseJsonEntity {

  public ImdbTextType node = null;
}
