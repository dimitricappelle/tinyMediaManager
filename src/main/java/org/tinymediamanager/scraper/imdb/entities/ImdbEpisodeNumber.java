package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ImdbEpisodeNumber extends BaseJsonEntity {
  public int episodeNumber = 0;
  public int seasonNumber  = 0;
}
