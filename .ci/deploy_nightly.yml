stages:
  - build
  - deploy

variables:
  MAVEN_CLI_OPTS: "-s .ci/settings.xml --batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  LFTP_OPTIONS: "set sftp:connect-program 'ssh -a -x -o StrictHostKeyChecking=no'; set net:timeout 5; set net:max-retries 2; set net:reconnect-interval-base 5;"

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .m2/repository/

build:
  stage: build
  image: tinymediamanager/packaging:20221121194626
  environment:
    name: nightly
    url: https://nightly.tinymediamanager.org
  allow_failure: false
  tags:
    - tmm # force running on own runners
  script:
    # generate changelog.txt
    - sh generate_changelog.sh

    # package archives
    - echo -e "\e[0Ksection_start:`date +%s`:maven_build\r\e[0KMaven build"
    - echo -n "$CODE_SIGN_CERT" | base64 -d > "code-sign-cert.p12"
    - mvn $MAVEN_CLI_OPTS -U -P dist -DbuildNumber=${CI_COMMIT_SHORT_SHA} -DskipSign=false -Dgetdown=getdown-nightly.txt -Dtier=nightly -Dthread_pool_size=1 clean package
    - echo -e "\e[0Ksection_end:`date +%s`:maven_build\r\e[0K"

    # build Windows installer
    - echo -e "\e[0Ksection_start:`date +%s`:windows_installer\r\e[0KWindows installer"
    - chmod 777 . dist
    - sudo -u xclient PATH=$PATH:/opt/bin AppBundler/create_windows_installer.sh "$CODE_SIGN_CERT" "$CODE_SIGN_PASS"
    - echo -e "\e[0Ksection_end:`date +%s`:windows_installer\r\e[0K"

    # push the build to the webserver
    - echo -e "\e[0Ksection_start:`date +%s`:ftp_upload\r\e[0KUpload"
    - lftp -c "${LFTP_OPTIONS} open -u ${FTP_USER_NIGHTLY},${FTP_PASSWORD_NIGHTLY} -p 23 sftp://${FTP_HOST}; mirror -Rev --parallel=2 target/tinyMediaManager-getdown/ ./upload/v5/build; mirror -Rev --parallel=2 dist/ ./upload/v5/dist; chmod -R g+rw ./upload/v5"
    - echo -e "\e[0Ksection_end:`date +%s`:ftp_upload\r\e[0K"

  artifacts:
    expire_in: 2 hours
    paths:
      - dist/tinyMediaManager*linux-amd64.tar.xz # upload for docker build
      - dist/tinyMediaManager*linux-arm64.tar.xz # upload for docker build
      - target/docker # compiled Dockerfile
      - target/tinyMediaManager*macos*.zip # for macOS build

build-mac:
  stage: build
  environment:
    name: nightly
    url: https://nightly.tinymediamanager.org
  needs:
    - build
  dependencies:
    - build
  allow_failure: false
  tags:
    - tmm-mac # force running on own macOS runners
  script:
    # remove linux build (no need to upload this twice)
    - rm -f dist/*

    # create macOS bundles
    - sh AppBundler/create_mac_image.sh

    # push the build to the webserver
    - lftp -c "${LFTP_OPTIONS} open -u ${FTP_USER_NIGHTLY},${FTP_PASSWORD_NIGHTLY} -p 23 sftp://${FTP_HOST}; mirror -Rv dist/ ./upload/v5/dist; chmod -R g+rw ./upload/v5"

deploy-docker-amd64:
  stage: deploy
  image:
    name: gcr.io/kaniko-project/executor:v1.9.1-debug
    entrypoint: [ "" ]
  variables:
    IMAGE_NAME: tinymediamanager/tinymediamanager
  needs:
    - build
  tags:
    - tmm # force running on own runners
  script:
    # create login credentials
    - echo -e $DOCKERHUB_AUTH > /kaniko/.docker/config.json

    # copy data
    - tar xJf dist/tinyMediaManager*linux-amd64.tar.xz -C target/docker/
    - cd target/docker

    # build image
    - VERSION=$(grep 'human.version' 'tinyMediaManager/version' | cut -d'=' -f2)
    - |
      /kaniko/executor \
      --context "${CI_PROJECT_DIR}/target/docker" \
      --dockerfile "${CI_PROJECT_DIR}/target/docker/Dockerfile.amd64" \
      --custom-platform=linux/amd64/ \
      --destination "${IMAGE_NAME}:5-nightly-amd64"

    #--destination "${IMAGE_NAME}:${$VERSION}" \

deploy-docker-arm64:
  stage: deploy
  image:
    name: gcr.io/kaniko-project/executor:v1.9.1-debug
    entrypoint: [ "" ]
  variables:
    IMAGE_NAME: tinymediamanager/tinymediamanager
  needs:
    - build
  tags:
    - tmm-arm64 # force running on own runners
  script:
    # create login credentials
    - echo -e $DOCKERHUB_AUTH > /kaniko/.docker/config.json

    # copy data
    - tar xJf dist/tinyMediaManager*linux-arm64.tar.xz -C target/docker/
    - cd target/docker
    - mv tinyMediaManager/tinyMediaManager-arm tinyMediaManager/tinyMediaManager

    # build image
    - VERSION=$(grep 'human.version' 'tinyMediaManager/version' | cut -d'=' -f2)
    - |
      /kaniko/executor \
      --context "${CI_PROJECT_DIR}/target/docker" \
      --dockerfile "${CI_PROJECT_DIR}/target/docker/Dockerfile.arm64" \
      --custom-platform=linux/arm64/ \
      --destination "${IMAGE_NAME}:5-nightly-arm64"

    #--destination "${IMAGE_NAME}:${$VERSION}" \

deploy-docker:
  stage: deploy
  image:
    name: curlimages/curl:8.1.1
  needs:
    - deploy-docker-amd64
    - deploy-docker-arm64
  dependencies: [ ]  # do not download any artifacts
  tags:
    - tmm # force running on own runners
  script:
    - cd /tmp
    - curl -s -L https://github.com/estesp/manifest-tool/releases/download/v2.0.8/binaries-manifest-tool-2.0.8.tar.gz | tar xvz
    - mv manifest-tool-linux-amd64 manifest-tool
    - chmod +x manifest-tool

    # create the manifest for multiarch tag
    - ./manifest-tool --username $DOCKERHUB_USERNAME --password $DOCKERHUB_PASSWORD push from-args
      --platforms linux/amd64,linux/arm64
      --template tinymediamanager/tinymediamanager:5-nightly-ARCH
      --target tinymediamanager/tinymediamanager:5-nightly
    - ./manifest-tool --username $DOCKERHUB_USERNAME --password $DOCKERHUB_PASSWORD push from-args
      --platforms linux/amd64,linux/arm64
      --template tinymediamanager/tinymediamanager:5-nightly-ARCH
      --target tinymediamanager/tinymediamanager:latest-nightly

deploy:
  stage: deploy
  image: curlimages/curl:7.85.0
  environment:
    name: nightly
    url: https://nightly.tinymediamanager.org
  dependencies: [ ]  # do not download any artifacts
  tags:
    - tmm # force running on own runners
  script:
    # and publish the files on the webserver
    - curl https://nightly.tinymediamanager.org/${PUBLISH_ENDPOINT_V5}
    - curl https://nightly.tinymediamanager.org/${CREATE_INDEX}
    - curl https://nightly.tinymediamanager.org/${CREATE_INDEX_V5}
